#ifndef GENERALIZEDDISTRIBUTION_H
#define GENERALIZEDDISTRIBUTION_H

#include <QTime>
#include <QDebug>
#include <QVector>
#include <QTextStream>
#include <QFile>
#include <QtGlobal>
#include <QDebug>
#include <QFileInfo>


#include <string>
#include <float.h>
#include <sstream>
#include <time.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <algorithm>
#include <map>
#include <functional>

#include <random>
#include <algorithm>
#include <vector>

#include "utility.h"


#define DELTAP 0.01
#define MINMM 0.01
#define MAXMM 1.0
#define ZEROMM 0.01

#define MAXBETA 4.0


class generalizedDistribution
{
public:
    generalizedDistribution();//Constructor
    generalizedDistribution(QString filename);//Constructor with argument：Input file name (csv) for time series data
    void setTimeSeriesData(QVector<double> ldata);//Set up time series data

    QString fileName();//get the input file name for time series data
    void estimate();//estimate
    bool save(QString filename="result.csv", bool isAppend=true);//save the results

    double shapeParameter();//get the shape parameter
    double exponentParameter();//get the exponent parameter

    double KSD();//get Kolmogorov-Smirnov statistic
    double likelihood();//get likelihood
    double AIC();//Akaike information criteria
    int lmin();//get the minimum value to be analysed
    int lmax();//get the maximum value to be analysed

    QVector<double> timeSeriesData();//get the time series data
    int usedN();//get the number of time series data used in the analysis
    int N();//get the number of time series data

    QVector<double> estimatedCCDF();//estimated CCDF
    QVector<double> observedCCDF();//observed CCDF
    QVector<double> estimatedFreq();//estimated frequency distribution
    QVector<double> observedFreq();//observed frequency distribution

private:

    Utility *util;
    void init();


    double KSTest(QVector<double> cumulative_freq, double cc, double shapeParameter,int xmin, int xmax,double *pp);
    double estimateParameter(int start,double *cc, double *shapeParameter, int *xmin, double *AIC,int *NN,double *minD);
    double estimateMaxLikelihoodParameter(QVector<double> freq,int xmin,int xmax,double *cc, double *shapeParameter, double *aic,int *NN,double *D);
    double logLikelihoodFunc(QVector<double> freq,int xmin, int xmax, double shapeParameter,double cc, int *NN);
    QVector<double> createCumulativeFreq(QVector<double> ldata,int xmin);
    QVector<double> createFreq(QVector<double> ldata);
    double estimatedFreq(double shapeParameter,double cc,double x);
    double cumulativeFreqFunc(int xmin, int xmax, double shapeParameter,double cc,double x);
    void createModelData();

    QString m_fileName;
    QVector<double> m_estimated_CCDF;
    QVector<double> m_observed_CCDF;
    QVector<double> m_observed_freq;
    QVector<double> m_estimated_freq;

    int m_N;
    double m_AIC;
    double m_minD;
    double m_maxL;
    double m_mm;
    double m_beta;
    int m_xmin;
    int m_xmax;


    QVector<double> m_ldata;
    QVector<double> m_cumulative_freq;
    QVector<double> m_freq;


};

#endif // GENERALIZEDDISTRIBUTION_H
