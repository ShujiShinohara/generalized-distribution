#include "generalizeddistribution.h"

generalizedDistribution::generalizedDistribution()
{
    init();
}


generalizedDistribution::generalizedDistribution(QString rfilename)
{
    init();
    m_fileName=rfilename;
    QVector< double > ldata=util->readFile(rfilename);
    setTimeSeriesData(ldata);
}
void generalizedDistribution::setTimeSeriesData(QVector<double> ldata)
{
    m_ldata.clear();

    for(int i=0;i<ldata.size();i++)
    {

        double val=ldata[i];
        if(val<=0)
            continue;

        m_ldata.append(val);


    }
    m_cumulative_freq=this->createCumulativeFreq(m_ldata,1);
    m_freq=this->createFreq(m_ldata);


    m_xmax=m_freq.size();

    m_N=m_ldata.size();

}




QString generalizedDistribution::fileName()
{
    return m_fileName;
}

//QVector<double> generalizedDistribution::freq()
//{
//    return m_freq;
//}

//QVector<double> generalizedDistribution::cumulativeFreq()
//{
//    return m_cumulative_freq;
//}

QVector<double> generalizedDistribution::timeSeriesData()
{
    return m_ldata;
}

QVector<double> generalizedDistribution::observedFreq()
{
    return m_observed_freq;
}

QVector<double> generalizedDistribution::estimatedFreq()
{
    return m_estimated_freq;
}


QVector<double> generalizedDistribution::estimatedCCDF()
{
    return m_estimated_CCDF;
}


QVector<double> generalizedDistribution::observedCCDF()
{
    return m_observed_CCDF;
}



double generalizedDistribution::likelihood()
{
    return m_maxL;
}

double generalizedDistribution::KSD()
{
    return m_minD;
}

double generalizedDistribution::AIC()
{
    return m_AIC;
}

double generalizedDistribution::shapeParameter()
{
    return m_mm;
}
double generalizedDistribution::exponentParameter()
{
    return m_beta;
}



int generalizedDistribution::N()
{
    return m_ldata.size();
}

int generalizedDistribution::usedN()
{
    return m_N;
}




int generalizedDistribution::lmin()
{
    return m_xmin;
}



int generalizedDistribution::lmax()
{
    return m_xmax;
}




void generalizedDistribution::init()
{

    util=new Utility;
    m_xmin=1;


    m_xmax=0;


    m_mm=0.0;
    m_beta=0.0;



    m_AIC=0.0;
    m_minD=0.0;
    m_maxL=0.0;

    m_ldata.clear();
    m_cumulative_freq.clear();

    m_estimated_CCDF.clear();
    m_observed_CCDF.clear();
}

void generalizedDistribution::estimate()
{
    estimateParameter(1, &m_beta, &m_mm, &m_xmin, &m_AIC,&m_N,&m_minD);
    createModelData();
}




void generalizedDistribution::createModelData()
{

    m_estimated_CCDF.clear();
    m_observed_freq.clear();
    m_estimated_freq.clear();
    m_observed_CCDF.clear();

    for(int i=0;i<m_cumulative_freq.size();i++)
    {
        m_observed_CCDF.append(m_cumulative_freq[i]/m_cumulative_freq[m_xmin-1]);

    }

    double sum_freq=0.0;
    double sum_freq_model=0.0;


    for(int i=(int)m_xmin-1;i<m_cumulative_freq.size();i++)
    {
        double x=(double)i+1.0;
        if(m_freq[i]>0.0)
        {
            sum_freq_model+=estimatedFreq(m_mm,m_beta,x);
            sum_freq+=m_freq[i];
        }
    }



    for(int i=0;i<m_cumulative_freq.size();i++)
    {
        double x=(double)i+1.0;

        double vv_cum_freq_model=0.0;


        double vv_freq=0.0;
        double vv_freq_model=0.0;
        if(sum_freq_model!=0.0)
            vv_freq_model=estimatedFreq(m_mm,m_beta,x)/sum_freq_model;


        vv_cum_freq_model=cumulativeFreqFunc(m_xmin,m_xmax,m_mm,m_beta,x);


        if(sum_freq!=0.0)
            vv_freq=m_freq[i]/sum_freq;


        m_estimated_CCDF.append(vv_cum_freq_model);

        m_observed_freq.append(vv_freq);
        m_estimated_freq.append(vv_freq_model);
    }

}



double generalizedDistribution::estimateParameter(int start,double *cc1, double *mm1, int *xmin, double *aic,int *NN,double *minD)
{
    *mm1=0.0;
    *cc1=0.0;
    *xmin=1;
    *NN=0;

    int N=0;

    *minD=1.0;
    double pp=0.0;
    int ct=0;
    double maxl=0.0;
    double lh=0.0;
    for(int i=start;i<m_xmax;i++)
    {

        double cc,mm;
        double taic=0.0;


        if(m_freq[i-1]<=0.0)
            continue;


        double D=0.0;

        lh=estimateMaxLikelihoodParameter(m_freq,i,m_xmax,&cc, &mm,&taic,&N, &D);


        if(ct==0)
        {
            *minD=D;
            *mm1=mm;
            *cc1=cc;
            *xmin=i;
            *aic=taic;
            *NN=N;
            maxl=lh;
        }
        else
        {
            if(D<*minD)
            {
                *minD=D;
                *mm1=mm;
                *cc1=cc;
                *xmin=i;
                *aic=taic;
                *NN=N;
                maxl=lh;

            }
        }
        ct++;
    }
    return maxl;
}



double generalizedDistribution::estimatedFreq(double mm,double beta,double x)
{
    double pp=0.0;

    double a=beta/mm;

    if(mm!=0.0)
        pp=beta*pow((double)x,mm-1.0)*exp(-a*pow((double)x,mm));
    else
        pp=pow((double)x,-beta);
    return pp;
}

double generalizedDistribution::cumulativeFreqFunc(int xmin, int xmax, double mm,double cc,double x)
{
    double pp=0.0;


    double a=cc/mm;
    double sum1=0.0;
    double sum0=0.0;
    int ix=(int)x;
    for(int i=1;i<=xmax;i++)
    {
        if(mm!=0.0)
        {
            if(i>=ix)
            {
                sum0+=cc*pow((double)i,mm-1.0)*exp(-a*pow((double)i,mm));
            }
            if(i>=xmin)
            {
                sum1+=cc*pow((double)i,mm-1.0)*exp(-a*pow((double)i,mm));
            }
        }
        else
        {
            if(i>=ix)
            {
                sum0+=pow((double)i,-cc);
            }
            if(i>=xmin)
            {
                sum1+=pow((double)i,-cc);
            }

        }
    }

    if(sum1!=0.0)
        pp=sum0/sum1;


    return pp;
}

double generalizedDistribution::logLikelihoodFunc(QVector<double> freq,int xmin, int xmax, double mm,double cc, int *N)
{

    double sum0=0.0;
    double a=cc/mm;
    *N=0;
    double C=0.0;
    for(int i=xmin;i<=xmax;i++)
    {
        C+=cc*pow((double)i,mm-1.0)*exp(-a*pow((double)i,mm));
        *N+=freq[i-1];
    }


    double lC=0.0;
    lC=-log(C)+log(cc);

    if(isinf(fabs(lC))  )
    {
        qDebug()<<"lC is inf "<<C<<" "<<xmin<<" "<<xmax<<" "<<*N;

    }

    for(int i=xmin;i<=xmax;i++)
    {
        if(freq[i-1]<=0.0)
            continue;

        sum0+=(double)freq[i-1]*((mm-1.0)*log((double)i)-a*pow((double)i,mm));
    }

    double sum=0.0;
    sum=(double)*N*lC+sum0;

    return sum;

}




double generalizedDistribution::estimateMaxLikelihoodParameter(QVector<double> freq,int xmin,int xmax,double *cc, double *mm, double *aic,int *NN,double *D)
{
    *cc=0.0;
    *mm=0.0;
    *aic=0.0;
    *D=0.0;
    double maxl=0.0;

    int ct=0;
    double sum=0.0;

    int N=0;
    *NN=0;
    int startMM=(int)((double)MINMM/(double)DELTAP);
    int endMM=(int)((double)MAXMM/(double)DELTAP);

    int startCC=1;
    int endCC=(int)((double)MAXBETA/(double)DELTAP);

    for(int im=startMM;im<=endMM;im++)
    {
        double mmm=(double)im*(double)DELTAP;
        if(mmm==0.0)
        {
            mmm=ZEROMM;
        }


        for(int ic=startCC;ic<=endCC;ic++)
        {
            double ccc=(double)ic*(double)DELTAP;

            sum=logLikelihoodFunc(freq,xmin, xmax, mmm,ccc,&N);

            if(isinf(fabs(sum))  )
            {
                qDebug()<<"inf "<<mmm<<" "<<ccc<<" "<<xmin<<" "<<xmax<<" "<<N;
                continue;
            }
            if(isnan(fabs(sum)) )
            {
                qDebug()<<"nan "<<mmm<<" "<<ccc<<" "<<xmin<<" "<<xmax<<" "<<N;
                continue;
            }


            if(ct==0)
            {
                maxl=sum;

                *mm=mmm;
                *cc=ccc;
                *aic=-2.0*maxl+4.0;
                *NN=N;
            }
            else
            {
                if(sum>maxl)
                {
                    maxl=sum;

                    *mm=mmm;
                    *cc=ccc;
                    *aic=-2.0*maxl+4.0;
                    *NN=N;
                }

            }
            ct++;

        }
    }

    QVector<double> cumulative_freq1;
    cumulative_freq1.fill(0.0,xmax);
    double sum11=m_cumulative_freq[xmin-1];
    if(sum11>0.0)
    {
        for(int j=xmin-1;j<m_xmax;j++)
        {
            cumulative_freq1[j]=m_cumulative_freq[j]/sum11;
        }
    }

    double pp=0.0;
    *D=KSTest(cumulative_freq1, *cc,*mm,xmin,cumulative_freq1.size(),&pp);

    return maxl;

}

double generalizedDistribution::KSTest(QVector<double> cumulative_freq, double cc, double mm,int xmin, int xmax,double *pp)
{
    double maxdiff=0.0;
    for(int x=xmin;x<=xmax;x++)
    {

        double vv=cumulativeFreqFunc(xmin,xmax,mm,cc,(double)x);


        double diff=fabs(vv-cumulative_freq[x-1]);
        if(diff>maxdiff)
        {
            maxdiff=diff;

        }
    }

    *pp=sqrt((double)m_ldata.size())*maxdiff;


    return maxdiff;
}




bool generalizedDistribution::save(QString filename,bool isAppend)
{

    std::string result_filename=filename.toStdString();
    std::string sep=",";
    std::ofstream out;

    bool exist=util->isExist(result_filename);

    if(isAppend)
        out.open( result_filename.c_str(), std::ios::out | std::ios::app );
    else
        out.open( result_filename.c_str());


    if(!exist || !isAppend)
    {
        out <<"filename";
        out<<sep<<"xmin";
        out<<sep<<"xmax";
        out<<sep<<"N";
        out<<sep<<"usedN";
        out<<sep<<"maxLikelihood";

        out<<sep<<"AIC";

        out<<sep<<"mm";
        out<<sep<<"beta";
        out <<std::endl;
    }


    out << fileName().toStdString();
    out <<sep<< lmin();
    out <<sep<< lmax();
    out <<sep<< N();
    out <<sep<< usedN();
    out <<sep<< likelihood();
    out <<sep<< AIC();
    out <<sep<< shapeParameter();
    out <<sep<< exponentParameter();



    out<<std::endl;



    out.close();


    return true;

}

QVector<double> generalizedDistribution::createCumulativeFreq(QVector<double> ldata,int xmin)
{

    QVector<double> freq;
    freq=createFreq(ldata);



    QVector<double> cumulative_freq;
    cumulative_freq.fill(0.0,freq.size());

    cumulative_freq[0]=ldata.size();

    for(int i=1;i<cumulative_freq.size();i++)
    {
        cumulative_freq[i]=(cumulative_freq[i-1]-freq[i-1]);
    }



    return cumulative_freq;
}

QVector<double> generalizedDistribution::createFreq(QVector<double> ldata)
{
    int max=1;

    for(int i=0;i<ldata.size();i++)
    {
        int val=(int)ldata[i];
        if(i==0)
            max=val;
        else
        {
            max=std::max(max,val);
        }
    }

    QVector<double> freq;
    freq.fill(0.0,max);


    for(int i=0;i<ldata.size();i++)
    {
        int val=(int)ldata[i];

        if(val>freq.size() || val<=0)
        {

            qDebug()<<"error"<<val<<","<<i<<","<<freq.size();
            qDebug()<<ldata;
        }

        freq[val-1]++;

    }


    return freq;
}


