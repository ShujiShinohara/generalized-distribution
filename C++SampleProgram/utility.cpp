﻿#include "utility.h"
using namespace std;

Utility::Utility()
{
    
}




Utility::~Utility()
{


}


std::string Utility::getCurrentDateTime()
{
    try
    {
        QDateTime dateTime = QDateTime::currentDateTime();
        std::string date_str=dateTime.toString("yyyy.MM.dd.hh.mm.ss").toStdString();

        return date_str;
    }
    catch(...)
    {
        std::cout<<"getCurrentDateTime Error"<<std::endl;
        return "";
    }
}
bool Utility::isExist(std::string filename)
{
    FILE* fp;

    fp = fopen(filename.c_str(), "r" );
    if( fp == NULL ){
        return false;
    }
    else{
        fclose( fp );
        return true;
    }

    return false;
}

QVector< QVector<double> > Utility::readCSV(QString fileName)
{
    QVector< QVector<double> > rt;
    rt.clear();

    QFile file(fileName);
    if (! file.open(QIODevice::ReadOnly)) {
             return rt;
    }
    QTextStream in(&file);



    while (! in.atEnd()) {

        QStringList list=in.readLine().split(",");

        QVector<double> tmp;
        tmp.clear();
        for(int i=0;i<list.size();i++)
        {
            QString str=list.at(i);
            double val=str.toDouble();
            tmp.append(val);
        }

        rt.append(tmp);

    }
    file.close();

    return rt;
}


QVector< double > Utility::readFile(QString rfilename)
{
    QVector<QVector< double >> data=this->readCSV(rfilename);
    QVector< double > ldata;
    ldata.clear();

    for(int i=0;i<data.size();i++)
    {
        double val=data[i][0];
        ldata.append(val);
    }

    return ldata;

}
void Utility::save2(QVector< QVector<double> > ary, QString filename)
{
    QFile file(filename);

    if (!file.open(QIODevice::WriteOnly))
    {
        qDebug()<<"Cannot open the output file.";
        return;
    }
    std::string sep=" ";
    QFileInfo info(filename);
    if(info.suffix()=="csv")
        sep=",";

    QTextStream out(&file);
    std::stringstream ss;

    for(int i=0;i<ary.size();i++)
    {
        for(int j=0;j<ary[i].size();j++)
        {
            if(j>0)
                ss<<sep<<ary[i][j];
            else
                ss<<ary[i][j];
        }
        ss<<std::endl;

    }

    out << ss.str().c_str();
    file.close();


}
void Utility::save1(QVector<double> ary, QString filename)
{
    QFile file(filename);

    if (!file.open(QIODevice::WriteOnly))
    {
        qDebug()<<"Cannot open the output file.";
        return;
    }
    QTextStream out(&file);
    std::stringstream ss;

    for(int i=0;i<ary.size();i++)
    {
        ss<<ary[i]<<std::endl;
    }

    out << ss.str().c_str();
    file.close();

}

double Utility::calc_mean(std::vector<double> dat)
{
    double mean = 0.0;

    if(dat.size()==0)
        return mean;

    for(int i=0;i<int(dat.size());i++)
    {
        mean+=dat.at(i);
    }
    if(dat.size()>0)
    {
        mean /= double(dat.size());
    }

    return mean;
}

QVector<double> Utility::calcAutoCorrelation(QVector<double> data,int tausize)
{

    QVector<double> cor;
    cor.clear();
    double val=0.0;
    for(int kk=0;kk<=tausize;kk++)
    {
        if(kk==0)
            val=1.0;
        else
        {
            QVector<double> y,x;
            x.clear();
            y.clear();

            for(int l=0;l<data.size()-kk;l++)
            {
                if(l+kk>=data.size())
                    break;

                x.append(data[l]);
                y.append(data[l+kk]);
            }



            val=calc_correlation(x.toStdVector(),y.toStdVector());

        }

        cor.append(val);
    }

    return cor;

}

double Utility::calc_correlation(std::vector<double> datx,std::vector<double> daty)
{
    if(datx.size()!=daty.size() || datx.size()==0)
        return 0.0;

    int n=datx.size();

    double mx = calc_mean(datx);
    double my = calc_mean(daty);

    double xx=0.0;
    double yy=0.0;
    double xy=0.0;

    for ( int i=0; i<n; i++ )
        xx += (datx.at(i)-mx)*(datx.at(i)-mx);

    xx = double(sqrt(xx));

    for ( int i=0; i<n; i++ )
        yy += (daty.at(i)-my)*(daty.at(i)-my);

    yy = double(sqrt(yy));

    for ( int i=0; i<n; i++ )
        xy += (datx.at(i)-mx)*(daty.at(i)-my);

    double nn=xx*yy;

    if(nn!=0.0)
        return (xy / nn);
    else
        return 0.0;
}

