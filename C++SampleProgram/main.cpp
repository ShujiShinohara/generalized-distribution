#include <QCoreApplication>


#include "utility.h"
#include "generalizeddistribution.h"


Utility *util;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);


    util=new Utility;

    generalizedDistribution *gd;

    QString filename="sample_length_time_series_data.csv";
    gd=new generalizedDistribution(filename);//Set up time series data
    gd->estimate();//estimate the parameters

    qDebug()<<"input filename="<<gd->fileName();//get the input file name for time series data
    qDebug()<<"N="<<gd->N();//get the number of time series data
    qDebug()<<"lmin="<<gd->lmin();//get the minimum value to be analysed
    qDebug()<<"lmax="<<gd->lmax();//get the maximum value to be analysed
    qDebug()<<"shape parameter="<<gd->shapeParameter();//get the shape parameter
    qDebug()<<"exponent parameter="<<gd->exponentParameter();//get the exponent parameter


    int tausize=100;//set the tau size
    QVector<double> auto_correlation;
    auto_correlation.clear();

    QVector<double> ldata=gd->timeSeriesData();//get the time series data
    auto_correlation=util->calcAutoCorrelation(ldata,tausize);//calculate the auto correlation

    for(int tau=0;tau<auto_correlation.size();tau++)
    {
        qDebug()<<"r["<<tau<<"]="<<auto_correlation[tau];
    }


    qDebug()<<"end";
    return a.exec();
}










