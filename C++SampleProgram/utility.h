﻿#ifndef Utility_H
#define Utility_H


#include <string>
#include <float.h>
#include <sstream>
#include <time.h>
#include <iostream>
#include <fstream>
#include <QTextStream>
#include <QFile>
#include <QtGlobal>
#include <QTime>
#include <QDebug>
#include <QVector>
#include <QFileInfo>
#include <sstream>
#include <random>

#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <math.h>
#include <algorithm>
#include <map>
#include <functional>
#include <string.h>

class Utility

{

public:

    Utility();
    ~Utility();


    std::string getCurrentDateTime();
    bool isExist(std::string filename);
    QVector< QVector<double> > readCSV(QString fileName);
    QVector< double > readFile(QString rfilename);
    double calc_mean(std::vector<double> dat);
    double calc_correlation(std::vector<double> datx,std::vector<double> daty);
    QVector<double> calcAutoCorrelation(QVector<double> data,int tausize);
    void save1(QVector<double> ary, QString filename);
    void save2(QVector< QVector<double> > ary, QString filename);
};

#endif // Utility_H
